# DataAccessSQLAssignment

This is mainly a project to test out the connection between a database and an application, created using a .NET Console Application and a SQL Express server using a sample Database by Chinook. Aswell as 9 scripts in the SQLqueries folder which you can use to create a Database unrelated to the Console Application and create different tables to manipulate.

## Usage
Go to Program.cs file and from line 11-19 you can pick which query-method you wish to try out by uncommenting them!

## Authors
#### [Carl-Johan Maelan (@carljohan.maelan)](@carljohan.maelan)
#### [Pontus Gillson (@d0senb0den)](@d0senb0den)


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
