USE [SuperHeroes]
GO

CREATE TABLE [dbo].[Superhero](
	[ID] [int] IDENTITY (1,1)NOT NULL,
	[Name] [nchar](100) NOT NULL,
	[Alias] [nchar](100) NOT NULL,
	[Origin] [nchar](100) NOT NULL,
 CONSTRAINT [PK_Superhero] PRIMARY KEY CLUSTERED ([ID])
 )
GO

CREATE TABLE [dbo].[Power](
	[ID] [int] IDENTITY (1,1) NOT NULL,
	[Name] [nchar](100) NOT NULL,
	[Description] [nchar](250) NOT NULL,
 CONSTRAINT [PK_Power] PRIMARY KEY CLUSTERED ([ID])
 )
GO
CREATE TABLE [dbo].[Assistant](
	[ID] [int] IDENTITY (1,1) NOT NULL,
	[Name] [nchar](100) NOT NULL,
 CONSTRAINT [PK_Assistant] PRIMARY KEY CLUSTERED ([ID])
 );
GO