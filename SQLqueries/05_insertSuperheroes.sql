INSERT INTO Superhero (Name, Alias, Origin)
VALUES ('Clark Kent', 'Superman', 'Krypton'),
('Bruce Wayne', 'Batman', 'Gotham'),
('Peter Parker', 'Spiderman', 'New York')

SELECT * FROM Superhero;