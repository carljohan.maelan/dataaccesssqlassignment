CREATE TABLE [dbo].[Superhero_Powers](
	[FK_Superhero_ID] [int] NULL,
	[FK_Power_ID] [int] NULL
) ON [PRIMARY]
GO

ALTER TABLE Superhero_Powers ADD  CONSTRAINT FK_Superhero_Powers FOREIGN KEY([FK_Power_ID])
REFERENCES Power([ID])
GO

ALTER TABLE Superhero_Powers  ADD  CONSTRAINT FK_Superhero FOREIGN KEY([FK_Superhero_ID])
REFERENCES Superhero([ID])
GO