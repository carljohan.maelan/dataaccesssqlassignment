INSERT INTO Power(Name, Description)
VALUES ('Money',  'Has alot of money'),
('Strength', 'Inhuman Strength'),
('Agility', 'Quick reflexes and nimble'),
('Flying', 'Possesses the ability to fly');
GO

SELECT * FROM Power;

INSERT INTO Superhero_Powers(FK_Power_ID)
SELECT ID
FROM Power

SELECT * FROM Superhero_Powers;

INSERT INTO Superhero_Powers(FK_Superhero_ID, FK_Power_ID)
VALUES (2,1),
(1, 2),
(1, 4),
(3, 3),
(3, 2)

SELECT * FROM Superhero_Powers;

SELECT * FROM Power

SELECT * FROM Superhero_Powers
JOIN Superhero ON Superhero.ID = Superhero_Powers.FK_Superhero_ID 
JOIN Power ON Power.ID = Superhero_Powers.FK_Power_ID;
